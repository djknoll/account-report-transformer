#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 22 21:06:10 2014

@author: dknoll
"""

import sys
import re
#import io
import os

if(len(sys.argv) != 2):
    print "usage: sb-art <input text file>"
    sys.exit(2)

inputFileName = sys.argv[1]
outputFileName = os.path.splitext(sys.argv[1])[0]+'.csv'

inputFile = open(inputFileName, "r")
#file = io.open(inputfile, "r", encoding='cp1252')
print "Reading from " + inputFileName

lineNr = 0
relevantLines = []
part = 0
for line in inputFile:
    lineNr += 1
    if(len(line) < 2):
        continue
    if(line[1] == '-'):
        part = (part + 1) % 3
        continue
    columnHeaders = part == 1 and len(relevantLines) < 4;
    if(columnHeaders or part == 2):
        relevantLines.append(line)

inputFile.close()

print "Analyzing"
columnSeparators = [19,25,33,40,67,83]
rows = []
for line in relevantLines:
    start = 0
    row = []
    for end in columnSeparators:
        column = line[start:end].strip()
        column = re.sub(' +', " ", column) # replace multiple spaces with only one
        row.append(column)
        start = end + 1
    lastColumn = line[start:].strip()
    #lastColumn = lastColumn.replace('.',',')
    if(lastColumn[-2:] == "CR"):     # if cell ents with CR it's +, we take it away und keep only the number
        lastColumn = lastColumn[:-2]
    elif(len(lastColumn.strip())>1): # if cell non empty prepend - sign
        lastColumn = ''.join(["-", lastColumn])
    row.append(lastColumn)
    
    if((len(rows)==1 and not row[3].isdigit()) or row[3] == ''):
        lastRow = rows.pop()
        row = [' '.join([oldColumn, column]).strip() for oldColumn, column in zip(lastRow, row)]
    rows.append(row)

beautifiedHeaders = []
headers = rows[0]
for column in headers:
    column = column.replace("|", "")
    column = column.replace("- ", "")
    column = column.replace("-", "")
    column = re.sub(' +', " ", column)
    beautifiedHeaders.append(column)


print "Writing to " + outputFileName
outputFile = open(outputFileName, "w")
#file = io.open(outputfile, "w", encoding="cp1252")

line = ';'.join(beautifiedHeaders)
outputFile.write(line)
outputFile.write("\n")

for row in rows[1:]:
    line = ';'.join(row)
    outputFile.write(line)
    outputFile.write("\n")
#    print line

outputFile.close()
